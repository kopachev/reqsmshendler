import json

from datetime import datetime
import requests
import time

from settings import SERVER_DO_TEST, PLACED_SERVER
import logging

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    variables_usage = 'Используемые значения\nCервер OTP = {otp_server},\nSMG-вебхук = {smg_web_hook}' \
        .format(**SERVER_DO_TEST)
    logger.warning(variables_usage)
    req_count = 0
    start_time = datetime.now()
    try:
        result = requests.post(
            SERVER_DO_TEST['smg_server'],
            headers={'Content-Type': 'application/json'},
            data=json.dumps({
                "phone_number": "79027722237",
                "sender": "585GOLD",
                "ttl": 30,
                "tag": "test",
                "callback_url": PLACED_SERVER
            })
        )
        print(
            'Выполнен запроса на генерирование пароля. Получен ответ {}'
                .format(result.text)
        )
        req_count += 1
    except requests.exceptions.ConnectionError:
        print('service nor avaliable')
    else:
        print('Запрос Выполнен, result', result)
