import json

from datetime import datetime
import requests
import time

from settings import SERVER_DO_TEST, PLACED_SERVER
import logging

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    variables_usage = 'Используемые значения\nCервер OTP = {otp_server},\nSMG-вебхук = {smg_web_hook}' \
        .format(**SERVER_DO_TEST)
    logger.warning(variables_usage)
    req_count = 0
    start_time = datetime.now()
    try:
        while True:
            try:
                result = requests.post(
                    SERVER_DO_TEST['otp_server'],
                    headers={'Content-Type': 'application/json'},
                    data=json.dumps({
                        "phone_number": "79116612887",
                        "sender": "585GOLD",
                        "ttl": 30,
                        "tag": "test",
                        "callback_url": PLACED_SERVER
                    })
                )
                print(
                    'Выполнен запроса на генерирование пароля. Получен ответ {}'
                        .format(result.text)
                )
                req_count += 1
            except requests.exceptions.ConnectionError:
                print('service nor avaliable')
            print('Спим....(выполнено {} запросов)'.format(req_count))
            print('\n')
            time.sleep(0.01)
    except KeyboardInterrupt:
        totaltime = datetime.now() - start_time
        print('Всего запросов {}'.format(req_count))
        print('Длительность тестов {:02}:{:02}:{:02} (в секундах {})'
              .format(totaltime.seconds // 3600, (totaltime.seconds // 60) % 60, (totaltime.seconds % 60 % 60),
                      totaltime.seconds))
        print('Запросов в секунду {}'.format(req_count / totaltime.seconds))
