import requests

import logging

logger = logging.getLogger(__name__)

def send_msg():
    params = {
        'clientId': '79027722237',
        'message': 'direct rrequest',
        'serviceId': '585gold_http_sandbox',
        'pass': 'cnZuDurr',
        'source': '585GOLD',
        'output': 'xml',
        'ptag': 'some_tag',
        'partnerMsgId': '18acc938-faf8-4519-b8c2-9dc19792b465',
        'msgType': 'SMS',
        'sending_time': None
    }
    # if message.get('delivery_time'):
    #     params['sending_time'] = message.get('delivery_time')

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    url = 'http://lk.zagruzka.com:9080/585gold_http_sandbox'
    print(f"use provider url {url} message {params['partnerMsgId']}")
    response = requests.post(url, data=params, headers=headers)
    print(f"{params['partnerMsgId']} {response.text}")
    print(f"{response}")
    return response

send_msg()