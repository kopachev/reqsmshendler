"""
    Обработчики эмулирующие работу провайдера
"""
import asyncio
import random

import requests
from aiohttp import web

from settings import SERVER_DO_TEST


async def send_delivery_status(provider_id, partnerMsgId):
    """
    msgType=SMS&partnerMsgId=5571dca9-d73f-4fbc-9c10-579da54c717f&transactionId=347293356&ptag=my-tag&status=2
    :return:

    """
    print('------------------')
    print('Обработка сообщения provider_id = {}, partnerMsgId = {}'.format(provider_id, partnerMsgId))
    params = {
        'transactionId': provider_id,
        'status': 2,
        'partnerMsgId': partnerMsgId,
        'msgType': 'SMS'
    }
    # rand_sec = random.randint(2, 16)
    rand_sec = 1
    await asyncio.sleep(rand_sec)

    try:
        res = requests.get(SERVER_DO_TEST['smg_web_hook'], params=params)
        print('Сообщение provider_id = {}, partnerMsgId = {} отправлено. Заказчик уведомлен status = {}'
              .format(provider_id, partnerMsgId, params.get('status')))
    except requests.ConnectionError:
        print('Заказчика Уведомить НЕ Удалось уведомлен Сообщение provider_id = {}, partnerMsgId = {} отправлено, '
              'status = {} '
              .format(provider_id, partnerMsgId, params.get('status')))
    print('------------------')


async def messages_provider_stub(request):
    """

    :param request:
    :return:
    """
    request_body = await request.post()
    partnerMsgId = request_body.get('partnerMsgId', '')
    print('------------------')
    print('Запрос на отправку сообщения!  partnerMsgId: {} uri: {}'.format(partnerMsgId, request.rel_url))
    request.app['provider_id'] += 1
    provider_id = request.app['provider_id']
    print('Присвоен id = {}'.format(provider_id))
    print('------------------')
    stub_response = \
        '<?xml version="1.0" encoding="utf-8"?>' \
        '<response>' \
        '<code>200</code>' \
        '<text>OK</text><payload>' \
        '<id>{}</id></payload>' \
        '</response>' \
            .format(provider_id)
    if partnerMsgId:
        asyncio.ensure_future(send_delivery_status(provider_id, partnerMsgId))
    return web.Response(text=stub_response)
