local_config = {
    'otp_server': 'http://localhost:5001/otp/pass',
    'smg_server': 'http://localhost:5000/otp/pass',
    'smg_web_hook': 'http://localhost:5000/sms/messages/webhook/provider'
}

dev_server_config = {
    'otp_server': 'http://10.170.20.17:8484/otp/pass',
    'smg_web_hook': 'http://10.170.20.17:8383/sms/messages/webhook/provider'
}

local_test_server_config = {
    'otp_server': 'http://10.170.20.17:8484/otp/pass',
    'smg_web_hook': 'http://localhost:8383/sms/messages/webhook/provider'
}

# Настройка указывающая где расположены тестируемые микросервисы.
# SERVER_DO_TEST = local_config
SERVER_DO_TEST = local_config

# Настройка указывающая где расположен тетстирующий сервис.
PLACED_SERVER = 'http://10.170.101.11:8080/webhook'
