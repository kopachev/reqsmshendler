"""
    Корневой модуль. Инициализация инстанса приложения.
"""
import uvloop
import asyncio
from aiohttp import web


from settings import SERVER_DO_TEST

import logging

from sms_provider_stubs import messages_provider_stub
from clients_handlers import index

logger = logging.getLogger(__name__)


def init_and_run():
    """
        Функция инициализации и запуска приложения.
    """
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    variables_usage = 'Используемые значения\nCервер OTP = {otp_server},\nSMG-вебхук = {smg_web_hook}' \
        .format(**SERVER_DO_TEST)
    logger.warning(variables_usage)

    loop = asyncio.get_event_loop()
    app = web.Application()
    app['request_counter'] = {
        's200': 0,
        's2': 0,
        's408': 0
    }
    app['provider_id'] = 100
    app.router.add_route('POST', r'/585gold_http_mob', messages_provider_stub)
    app.router.add_route('POST', r'/585gold_http_sandbox', messages_provider_stub)
    app.router.add_route('*', r'/{path:.*}', index)
    web.run_app(app, host='0.0.0.0', port=8080, loop=loop)
