from xml.etree import ElementTree as etree
class XMLBody:

    def __init__(self, xml_source):
        self.xml = xml_source
        self.response_root = etree.fromstring(self.xml)

    def get_node(self, node_name):
        return self.response_root.find(node_name)

    @property
    def code(self):
        code = self.get_node('code')
        if code is not None:
            return int(code.text)

    @property
    def text(self):
        text = self.get_node('text')
        if text is not None:
            return text.text

    @property
    def id(self):
        payload = self.get_node('payload')
        if payload is not None and payload.find('id') is not None:
            return payload.find('id').text

    @property
    def success(self):
        return self.code == 200

    @property
    def serialized_data(self):
        return {'id': self.id, 'code': self.code, 'text': self.text}

    def __str__(self):
        return f"{self.code} {self.text}"
