"""
    Точка входа. Модуль управления приложением.
"""
from main import init_and_run

if __name__ == "__main__":
    init_and_run()
