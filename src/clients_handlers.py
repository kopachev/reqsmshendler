"""
    Обработчики уведомлений клиента
"""
from aiohttp import web


async def index(request):
    """
        Общий обработчик клиентского вебхука.
    """

    # main_logger.info('Принят запрос. request '.format(request))
    request_body = await request.text()
    try:
        json_body = await request.json()
    except ValueError:
        pass
    else:
        if str(json_body.get('status')) == '200':
            request.app['request_counter']['s200'] += 1
        elif str(json_body.get('status')) == '2':
            request.app['request_counter']['s2'] += 1
        elif str(json_body.get('status')) == '408':
            request.app['request_counter']['s408'] += 1

    # Q.put_nowait("sssssss")

    log_msg = 'Принят ответ для клиента . на: {},  uri: {},  body: {}'.format(request.host, request.rel_url, request_body)
    print(log_msg)
    request_counter = request.app['request_counter']
    print('----------------- Всего ответов 200: {s200}; 2: {s2}; 408:{s408} ----------------'.format(**request_counter))
    return web.Response(text='Web_hook received!')
